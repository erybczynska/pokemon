const express = require('express');
const app = express();

app.set('port', (process.env.PORT || 5000));


const buildRoot = __dirname + '/build';
app.use(express.static(buildRoot));

const indexFile = buildRoot + '/index.html';
app.use('/', express.static(indexFile));
app.use('/pokemon/*', express.static(indexFile));


app.listen(app.get('port'), function () {
  console.log('Node app is running on port', app.get('port'));
});
