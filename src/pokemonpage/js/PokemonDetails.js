import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Table} from "react-bootstrap";
import "../style/PokemonDetails.scss";

class PokemonDetails extends Component {

  render() {
    const {pokemonData} = this.props;

    return (
        <div className="PokemonDetails">
          <Table>
            <thead>
            <tr>
              <th>Speed</th>
              <th>Attack</th>
              <th>Defense</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>{pokemonData && pokemonData.speed}</td>
              <td>{pokemonData && pokemonData.attack}</td>
              <td>{pokemonData && pokemonData.defense}</td>
            </tr>
            </tbody>
          </Table>
        </div>
    );
  }
}

PokemonDetails.propTypes = {
  pokemonDetails: PropTypes.object
};

export default PokemonDetails;
