import React, {Component} from 'react';
import {Col, Grid, Row} from "react-bootstrap";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {
  fetchPokemonIfNeeded,
  fetchEvolutionFormsIfNeeded
} from "../../redux/actions";
import "../style/PokemonPage.scss";
import PokemonDetails from "./PokemonDetails";
import PokemonForms from "./PokemonForms";
import Loader from "../../util/js/Loader";

class PokemonPage extends Component {

  componentDidMount() {
    const {dispatch} = this.props;
    dispatch(fetchPokemonIfNeeded(this.getPokemonId(this.props)));
    dispatch(fetchEvolutionFormsIfNeeded(this.getPokemonId(this.props)));
  }

  componentWillReceiveProps(nextProps) {
    const {dispatch} = nextProps;
    dispatch(fetchPokemonIfNeeded(this.getPokemonId(nextProps)));
    dispatch(fetchEvolutionFormsIfNeeded(this.getPokemonId(nextProps)));
  }

  getPokemonId = props => {
    const {match} = props;
    return Number(match.params.pokemonId);
  };

  render() {
    const pokemonId = this.getPokemonId(this.props);
    const {pokemonsDetails, pokemonsForms} = this.props;
    const pokemonWrapper = pokemonsDetails[pokemonId];
    const isFetching = pokemonWrapper && pokemonWrapper.isFetching;
    const error = pokemonWrapper && pokemonWrapper.error;
    const pokemonData = pokemonWrapper && pokemonWrapper.data;

    return (
        <div className="PokemonPage">
          <Grid>
            <Row>
              <h2>{pokemonData && pokemonData.name}</h2>
            </Row>
            <Row>
              <Col md={2}>
                {pokemonData && <img src={pokemonData.image}
                                     alt={pokemonData.name}/>}

              </Col>
              <Col md={6}>
                {isFetching && <Loader/>}
                {error && 'error'}
                {pokemonData && <PokemonDetails pokemonData={pokemonData}/>}
              </Col>
              <Col md={4}>
                <PokemonForms pokemonsForms={pokemonsForms}
                              pokemonId={pokemonId}/>
              </Col>
            </Row>
            <Row>
              <Link to="/" className="back-button">Back to table</Link>
            </Row>
          </Grid>
        </div>
    );
  }
}

PokemonPage.propTypes = {
  pokemonsDetails: PropTypes.object.isRequired,
  pokemonsForms: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    pokemonsDetails: state.pokemon.byId,
    pokemonsForms: state.pokemon.evolutionFormsById
  }
};

PokemonPage = connect(mapStateToProps)(PokemonPage);

export default PokemonPage;