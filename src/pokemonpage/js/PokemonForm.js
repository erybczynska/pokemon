import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

class PokemonForm extends Component {

  render() {
    const {form} = this.props;

    return (
        <li>
          <Link to={"/pokemon/" + form.id}>{form.name}</Link>
        </li>
    );
  }
}

PokemonForm.propTypes = {
  form: PropTypes.object.isRequired
};

export default PokemonForm;