import React, {Component} from 'react';
import PropTypes from "prop-types";
import PokemonForm from "./PokemonForm";
import "../style/PokemonForms.scss";
import Loader from "../../util/js/Loader";

class PokemonForms extends Component {

  render() {
    const {pokemonsForms, pokemonId} = this.props;
    const pokemonFormsWrapper = pokemonsForms[pokemonId];
    const isFetching = pokemonFormsWrapper && pokemonFormsWrapper.isFetching;
    const error = pokemonFormsWrapper && pokemonFormsWrapper.error;
    const forms = pokemonFormsWrapper && pokemonFormsWrapper.data;

    return (
        <div className="PokemonForms">
          <h4>Evolution forms</h4>
          <ul>
            {isFetching && <Loader/>}
            {error && 'error'}
            {forms && forms.filter(form => form.id != pokemonId).map(
                form => <PokemonForm key={form.id} form={form}/>)}
          </ul>
        </div>
    );
  }
}

PokemonForms.propTypes = {
  pokemonsForms: PropTypes.object,
  pokemonId: PropTypes.number.isRequired
};

export default PokemonForms;