import {
  REQUEST_POKEMON_PAGE,
  RECEIVE_POKEMON_PAGE,
  POKEMON_PAGE_ERROR,
  REQUEST_POKEMON,
  RECEIVE_POKEMON,
  POKEMON_ERROR,
  REQUEST_EVOLUTION_FORMS,
  RECEIVE_EVOLUTION_FORMS,
  EVOLUTION_FORMS_ERROR
} from "./actionTypes";
import client from "../util/js/client";

export function fetchPokemonPageIfNeeded(pageNumber) {
  return (dispatch, getState) => {
    if (shouldFetchPokemonPage(getState(), pageNumber)) {
      return dispatch(fetchPokemonPage(pageNumber));
    }
    return Promise.resolve();
  }
}

function shouldFetchPokemonPage(state, pageNumber) {
  const pageWrapper = state.pokemonPage.byId[pageNumber];
  return shouldFetchObject(pageWrapper);
}

function shouldFetchObject(object) {
  if (!object) {
    return true;
  } else if (object.isFetching) {
    return false;
  } else if (object.error) {
    return false;
  }
  return !object.data;
}

function fetchPokemonPage(pageNumber) {
  return dispatch => {
    dispatch(requestPokemonPage(pageNumber));

    const offset = (pageNumber - 1) * 10;
    return client('https://pokeapi.co/api/v2/pokemon/?limit=10&offset=' + offset)
      .then(response => response.entity)
      .then(pokemonPage => dispatch(receivePokemonPage(pageNumber, pokemonPage)))
      .catch(response => dispatch(
          pageRequestError(pageNumber, response)));
  }
}

function requestPokemonPage(pageNumber) {
  return {
    type: REQUEST_POKEMON_PAGE,
    pageNumber
  }
}

function receivePokemonPage(pageNumber, pokemonPage) {
  return {
    type: RECEIVE_POKEMON_PAGE,
    pageNumber,
    pokemonPage
  }
}

function pageRequestError(pageNumber) {
  return {
    type: POKEMON_PAGE_ERROR,
    pageNumber
  }
}

export function fetchPokemonIfNeeded(id) {
  return (dispatch, getState) => {
    if (shouldFetchPokemon(getState(), id)) {
      return dispatch(fetchPokemon(id));
    }
    return Promise.resolve();
  }
}

function shouldFetchPokemon(state, pokemonId) {
  const pokemonWrapper = state.pokemon.byId[pokemonId];
  return shouldFetchObject(pokemonWrapper);
}

function fetchPokemon(id) {
  return dispatch => {
    dispatch(requestPokemon(id));

    return client('https://pokeapi.co/api/v2/pokemon/' + id + '/')
      .then(response => response.entity)
      .then(pokemon => dispatch(receivePokemon(id, pokemon)))
      .catch(response => dispatch(
          pokemonRequestError(id, response)));
  }
}

function requestPokemon(id) {
  return {
    type: REQUEST_POKEMON,
    pokemonId: id
  }
}

function receivePokemon(id, pokemon) {
  return {
    type: RECEIVE_POKEMON,
    pokemonId: id,
    pokemon
  }
}

function pokemonRequestError(id) {
  return {
    type: POKEMON_ERROR,
    pokemonId: id
  }
}

export function fetchEvolutionFormsIfNeeded(pokemonId) {
  return (dispatch, getState) => {
    if (shouldFetchEvolutionForms(getState(), pokemonId)) {
      return dispatch(fetchEvolutionForms(pokemonId));
    }
    return Promise.resolve();
  }
}

function shouldFetchEvolutionForms(state, pokemonId) {
  const evolutionFormsWrapper = state.pokemon.evolutionFormsById[pokemonId];
  return shouldFetchObject(evolutionFormsWrapper);
}

function fetchEvolutionForms(id) {
  return dispatch => {
    dispatch(requestEvolutionForms(id));

    return client('https://pokeapi.co/api/v2/pokemon-species/' + id + '/')
      .then(response => response.entity)
      .then(species => species.evolution_chain.url)
      .then(evolutionChainUrl => evolutionChainUrl.match(/evolution-chain\/(\d+)/)[1])
      .then(chainId => client('https://pokeapi.co/api/v2/evolution-chain/' + chainId + '/'))
      .then(response => response.entity.chain)
      .then(evolutionChain => dispatch(receiveEvolutionForms(id, evolutionChain)))
      .catch(response => dispatch(evolutionFormsError(id, response)));
  }
}

function requestEvolutionForms(id) {
  return {
    type: REQUEST_EVOLUTION_FORMS,
    pokemonId: id
  }
}

function receiveEvolutionForms(id, evolutionChain) {
  return {
    type: RECEIVE_EVOLUTION_FORMS,
    pokemonId: id,
    evolutionChain
  }
}

function evolutionFormsError(id) {
  return {
    type: EVOLUTION_FORMS_ERROR,
    pokemonId: id
  }
}