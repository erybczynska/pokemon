import {combineReducers} from "redux";
import {
  REQUEST_POKEMON,
  RECEIVE_POKEMON,
  POKEMON_ERROR,
  EVOLUTION_FORMS_ERROR,
  RECEIVE_EVOLUTION_FORMS,
  REQUEST_EVOLUTION_FORMS
} from "./actionTypes";

function byId(state = {}, action) {
  switch (action.type) {
    case REQUEST_POKEMON:
    case RECEIVE_POKEMON:
    case POKEMON_ERROR:
      return Object.assign({}, state, {
        [action.pokemonId]: handlePokemon(action)
      });
    default:
      return state;
  }
}

function handlePokemon(action) {
  switch (action.type) {
    case REQUEST_POKEMON:
      return {
        isFetching: true,
        error: false
      };
    case RECEIVE_POKEMON:
      return {
        isFetching: false,
        data: mapPokemon(action.pokemon),
        error: false
      };
    case POKEMON_ERROR:
      return {
        isFetching: false,
        error: true
      };
    default:
      return state;
  }
}

function mapPokemon(pokemon) {
  return {
    name: pokemon.name,
    image: pokemon.sprites.front_default,
    hp: findStat(pokemon, "hp"),
    speed: findStat(pokemon, "speed"),
    attack: findStat(pokemon, "attack"),
    defense: findStat(pokemon, "defense")
  };
}

function findStat(pokemon, statName) {
  return pokemon.stats.find(stat => stat.stat.name === statName).base_stat;
}

function evolutionFormsById(state = {}, action) {
  switch (action.type) {
    case REQUEST_EVOLUTION_FORMS:
    case RECEIVE_EVOLUTION_FORMS:
    case EVOLUTION_FORMS_ERROR:
      return Object.assign({}, state, {
        [action.pokemonId]: handleEvolutionForms(action)
      });
    default:
      return state;
  }
}

function handleEvolutionForms(action) {
  switch (action.type) {
    case REQUEST_EVOLUTION_FORMS:
      return {
        isFetching: true,
        error: false
      };
    case RECEIVE_EVOLUTION_FORMS:
      return {
        isFetching: false,
        data: mapEvolutionChain(action.evolutionChain),
        error: false
      };
    case EVOLUTION_FORMS_ERROR:
      return {
        isFetching: false,
        error: true
      };
    default:
      return state;
  }
}

function mapEvolutionChain(evolutionChain) {
  const evolutionForms = [];
  addEvolutionForm(evolutionForms, evolutionChain);
  return evolutionForms;
}

function addEvolutionForm(evolutionForms, evolutionForm) {
  if (!evolutionForm.species) {
    return;
  }
  evolutionForms.push({
    id: evolutionForm.species.url.match(/pokemon-species\/(\d+)/)[1],
    name: evolutionForm.species.name
  });
  evolutionForm.evolves_to
    .forEach(evolutionForm => addEvolutionForm(evolutionForms, evolutionForm));
}

const pokemon = combineReducers({
  byId,
  evolutionFormsById
});

export default pokemon;