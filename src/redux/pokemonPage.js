import {combineReducers} from "redux";
import {
  REQUEST_POKEMON_PAGE,
  RECEIVE_POKEMON_PAGE,
  POKEMON_PAGE_ERROR
} from "./actionTypes";

function byId(state = {}, action) {
  switch (action.type) {
    case REQUEST_POKEMON_PAGE:
    case RECEIVE_POKEMON_PAGE:
    case POKEMON_PAGE_ERROR:
      return Object.assign({}, state, {
        [action.pageNumber]: handlePage(action)
      });
    default:
      return state;
  }
}

function handlePage(action) {
  switch (action.type) {
    case REQUEST_POKEMON_PAGE:
      return {
        isFetching: true,
        error: false
      };
    case RECEIVE_POKEMON_PAGE:
      return {
        isFetching: false,
        data: action.pokemonPage.results.map(pokemon => mapPokemonUrlToId(pokemon)),
        error: false
      };
    case POKEMON_PAGE_ERROR:
      return {
        isFetching: false,
        error: true
      };
    default:
      return state;
  }
}

function mapPokemonUrlToId(pokemon) {
  return {
    name: pokemon.name,
    id: Number(pokemon.url.match(/pokemon\/(\d+)/)[1])
  };
}

function pokemonCount(state = -1, action) {
  switch (action.type) {
    case RECEIVE_POKEMON_PAGE:
      return action.pokemonPage.count;
    default:
      return state;
  }
}

const pokemonPage = combineReducers({
  byId,
  pokemonCount
});

export default pokemonPage;