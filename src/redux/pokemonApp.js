import {combineReducers} from 'redux';
import pokemon from "./pokemon";
import pokemonPage from "./pokemonPage";

const pokemonApp = combineReducers({
  pokemon,
  pokemonPage
});

export default pokemonApp;