import reducer from '../pokemonPage'
import {
  REQUEST_POKEMON_PAGE,
  RECEIVE_POKEMON_PAGE,
  POKEMON_PAGE_ERROR
} from '../actionTypes'

describe('pokemonPage reducer', () => {

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
        {
          byId: {},
          pokemonCount: -1
        }
    )
  });

  it('should update pokemon count and add page', () => {
    expect(reducer(undefined, {
          type: RECEIVE_POKEMON_PAGE,
          pageNumber: 2,
          pokemonPage: {
            count: 7,
            results: [
              {name: 'one', url: 'http://domain/pokemon/1'},
              {name: 'two', url: 'http://domain/pokemon/2'},
              {name: 'three', url: 'http://domain/pokemon/3'},
            ]
          }
        }
    )).toEqual(
        {
          byId: {
            2: {
              isFetching: false,
              error: false,
              data: [
                {
                  name: 'one',
                  id: 1
                },
                {
                  name: 'two',
                  id: 2
                },
                {
                  name: 'three',
                  id: 3
                }]
            }
          },
          pokemonCount: 7
        }
    )
  })

  it('should set page fetching', () => {
    expect(reducer(undefined, {
          type: REQUEST_POKEMON_PAGE,
          pageNumber: 2
        }
    )).toEqual(
        {
          byId: {
            2: {
              isFetching: true,
              error: false
            }
          },
          pokemonCount: -1
        }
    )
  });

  it('should set page request error', () => {
    expect(reducer(undefined, {
          type: POKEMON_PAGE_ERROR,
          pageNumber: 2
        }
    )).toEqual(
        {
          byId: {
            2: {
              isFetching: false,
              error: true
            }
          },
          pokemonCount: -1
        }
    )
  });

});