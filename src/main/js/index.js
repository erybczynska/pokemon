import React from 'react';
import {render} from "react-dom";
import {applyMiddleware, createStore} from 'redux';
import thunkMiddleware from "redux-thunk";
import App from './App';
import pokemon from "../../redux/pokemonApp";

const middleware = process.env.NODE_ENV !== 'production' ?
    applyMiddleware(thunkMiddleware, require("redux-logger").createLogger())
    : applyMiddleware(thunkMiddleware);

const store = createStore(pokemon, middleware);

render(
    <App store={store}/>,
    document.getElementById('root')
);

if (module.hot) {
  module.hot.accept('./App', () => {
    const AppContainer = require('./App').default;
    render(
        <AppContainer store={ store }/>,
        document.getElementById('root')
    );
  });
}