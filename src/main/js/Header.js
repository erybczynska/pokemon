import React, {Component} from "react";
import "../style/Header.scss";
import {Link} from "react-router-dom";
import {Col, Grid, Row} from "react-bootstrap";

class Header extends Component {
  render() {
    return (
        <header className="Header">
          <Grid>
            <Row>
              <Col md={6}>
                <div className="nav">
                  <Link to="/" className="pokemon">Pokemon Cheat Sheet</Link>
                </div>
              </Col>
              <Col md={6}/>
            </Row>
          </Grid>
        </header>

    );
  }
}

export default Header;