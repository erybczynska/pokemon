import React, {Component} from 'react';
import "../style/Footer.scss";
import {Col, Grid, Row} from "react-bootstrap";

class Footer extends Component {

  render() {
    const year = new Date().getFullYear();

    return (
        <footer className="Footer">
          <Grid>
            <Row>
              <Col md={4}>
                <p>Ewelina Rybczyńska</p>
              </Col>
              <Col md={4}>
                <p>© {year} Copyright Pokemon Cheat Sheet</p>
              </Col>
              <Col md={4}>
                <p>Contact</p>
              </Col>
            </Row>
          </Grid>
        </footer>
    );
  }
}

export default Footer;