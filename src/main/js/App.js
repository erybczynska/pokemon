import React, {Component} from 'react';
import PropTypes from "prop-types";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {Provider} from "react-redux";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/scss/font-awesome.scss";
import "../style/App.scss";
import LayoutWrapper from "./LayoutWrapper";
import PokemonTable from "../../table/js/PokemonTable";
import PokemonPage from "../../pokemonpage/js/PokemonPage";

class App extends Component {

  render() {
    const {store} = this.props;

    return (
        <Provider store={store}>
          <Router history={history}>
            <LayoutWrapper>
              <Switch>
                <Route exact path="/" component={PokemonTable}/>
                <Route path="/pokemon/:pokemonId" component={PokemonPage}/>
                <Route path="/:pageId" component={PokemonTable}/>
              </Switch>
            </LayoutWrapper>
          </Router>
        </Provider>
    );
  }
}

App.propTypes = {
  store: PropTypes.object.isRequired
};

export default App;