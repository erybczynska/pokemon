import React, {Component} from "react";
import Footer from "./Footer";
import Header from "./Header";

class LayoutWrapper extends Component {
  render() {
    return (
        <div>
          <Header/>
          <main>
            {this.props.children}
          </main>
          <Footer/>
        </div>
    );
  }
}

export default LayoutWrapper;