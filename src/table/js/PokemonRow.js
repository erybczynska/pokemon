import React, {Component} from 'react';
import PropTypes from "prop-types";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {fetchPokemonIfNeeded} from "../../redux/actions";
import Loader from "../../util/js/Loader";


class PokemonRow extends Component {

  componentDidMount() {
    const {pokemon, dispatch} = this.props;
    dispatch(fetchPokemonIfNeeded(pokemon.id));
  }

  click = () => {
    const {history, pokemon} = this.props;
    history.push('/pokemon/' + pokemon.id);
  };

  render() {
    const {pokemon, pokemonsDetails} = this.props;
    const pokemonWrapper = pokemonsDetails[pokemon.id];
    const isFetching = pokemonWrapper && pokemonWrapper.isFetching;
    const error = pokemonWrapper && pokemonWrapper.error;
    const pokemonData = pokemonWrapper && pokemonWrapper.data;

    return (
        <tr onClick={this.click}>
          <td>
            {isFetching && <Loader/>}
            {error && 'error'}
            {pokemonData && <img src={pokemonData.image} alt={pokemon.name}/>}
          </td>
          <td>{pokemon.name}</td>
          <td>
            {isFetching && <Loader/>}
            {error && 'error'}
            {pokemonData && pokemonData.hp}
          </td>
        </tr>
    );
  }
}

PokemonRow.propTypes = {
  pokemon: PropTypes.object.isRequired,
  pokemonsDetails: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    pokemonsDetails: state.pokemon.byId,
  }
};

PokemonRow = connect(mapStateToProps)(PokemonRow);

export default withRouter(PokemonRow);
