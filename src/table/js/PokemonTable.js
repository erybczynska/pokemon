import React, {Component} from 'react';
import Pager from "react-pager";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {Row, Table} from "react-bootstrap";
import "../style/PokemonTable.scss";
import {fetchPokemonPageIfNeeded} from "../../redux/actions";
import PokemonRow from "./PokemonRow";
import Loader from "../../util/js/Loader";

class PokemonTable extends Component {

  componentDidMount() {
    const pageNumber = this.getPageNumber(this.props.match);
    this.fetchPage(this.props, pageNumber);
  }

  getPageNumber(match) {
    let pageId = Number(match.params.pageId);
    if (isNaN(pageId)) {
      pageId = 1;
    }
    return pageId;
  }

  fetchPage = (props, pageNumber) => {
    const {dispatch} = props;
    dispatch(fetchPokemonPageIfNeeded(pageNumber));
  };

  changePage = pageNumberFromZero => {
    const {history} = this.props;
    const pageNumber = pageNumberFromZero + 1;
    history.push('/' + pageNumber);
    this.fetchPage(this.props, pageNumber);
  };

  render() {
    const {match, count, pages} = this.props;

    const pageNumber = this.getPageNumber(match);
    const pokemonsWrapper = pages[pageNumber];
    const isFetching = pokemonsWrapper && pokemonsWrapper.isFetching;
    const error = pokemonsWrapper && pokemonsWrapper.error;
    let pokemons = pokemonsWrapper && pokemonsWrapper.data;
    pokemons = pokemons || [];
    const pageCount = count > 0 && Math.floor(count / 10) + 1;

    const rows = pokemons.map(
        pokemon => <PokemonRow key={pokemon.id} pokemon={pokemon}/>);

    return (
        <div className="PokemonTable">
          <Row>
            <Table>
              <thead>
              <tr>
                <th>Image</th>
                <th>Name</th>
                <th>HP</th>
              </tr>
              </thead>
              <tbody>
              {!isFetching && rows}
              </tbody>
            </Table>
            {isFetching && <Loader/>}
          </Row>

          <div className="page-numeration">
            {pageCount && <Pager
                total={pageCount}
                current={pageNumber - 1}
                visiblePages={8}
                onPageChanged={this.changePage}
                className="pager-custom"
            />}
            {error && 'error!'}
          </div>
        </div>
    );
  }
}

PokemonTable.propTypes = {
  pages: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired
};

const mapStateToProps = state => {
  return {
    pages: state.pokemonPage.byId,
    count: state.pokemonPage.pokemonCount
  }
};

PokemonTable = connect(mapStateToProps)(PokemonTable);

export default withRouter(PokemonTable);