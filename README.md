# Pokemon cheat sheet #

### Technologies used in project ###

* ES6
* React + Redux
* Sass
* jest (testing)
* webpack (build)

### How to setup ###

* download dependencies and build `npm i`
* build only `npm run build`
* run `node node_start.js`
* run dev server `npm run dev`
* run test `npm run test`